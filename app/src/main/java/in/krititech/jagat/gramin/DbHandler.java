package in.krititech.jagat.gramin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

public class DbHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "recordManager";

    // Contacts table name
    private static final String TABLE_RECORDS = "records";
    private static final String TABLE_SONGS = "songs";
    private static final String TABLE_PLAYLIST = "playlist";
    private static final String TABLE_STATS = "gpstats";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";
    private static final String KEY_TIME = "time";
    private static final String KEY_TYPE = "type";
    private static final String KEY_STATUS = "status";
    private static final String KEY_EVENTID = "eventid";
    private static final String KEY_DURATION = "duration";
    private static final String KEY_MTD = "mtd";
    private static final String KEY_YTD = "ytd";
    private static final String KEY_UPD = "upd";
    private static final String KEY_DEVICEID = "deviceid";

    private static final String KEY_SONGNAME = "songname";
    private static final String KEY_SONGID = "songid";
    private static final String KEY_SONG_ID = "id";


    private static final String KEY_PLAYLIST_ID = "id";
    private static final String KEY_PLAYLIST_DATE = "date";
    private static final String KEY_PLAYLIST_PATH = "path";
    private static final String KEY_PLAYLIST_SONGNAME = "songname";
    private static final String KEY_PLAYLIST_DURATION = "duration";
    private static final String KEY_PLAYLIST_TYPE = "type";
    private static final String KEY_PLAYLIST_SVRID = "playlistid";
    private static final String KEY_PLAYLIST_STARTTIME = "starttime";
    private static final String KEY_PLAYLIST_ENDTIME = "endtime";

    private static final String KEY_STATS_ID = "id";
    private static final String KEY_STATS_NAME = "key";
    private static final String KEY_STATS_VALUE= "val";


    private final ArrayList<Datarecord> datarecords = new ArrayList<Datarecord>();
    private final ArrayList<Datasong> datasongs = new ArrayList<Datasong>();
    private final ArrayList<Dataplaylist> dataplaylists = new ArrayList<Dataplaylist>();
    private final ArrayList<Datastats> datastats = new ArrayList<Datastats>();

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_DATA_RECORD = "CREATE TABLE " + TABLE_RECORDS + "("
                + KEY_ID + " INTEGER PRIMARY KEY autoincrement," + KEY_DEVICEID + " TEXT,"+ KEY_DATE + " TEXT,"
                + KEY_TIME + " TEXT," + KEY_TYPE + " TEXT," +  KEY_STATUS + " TEXT," +  KEY_EVENTID + " TEXT," +
                KEY_DURATION + " TEXT," + KEY_MTD + " TEXT," + KEY_YTD + " TEXT,"+ KEY_UPD + " INTEGER" +")";
        db.execSQL(CREATE_DATA_RECORD);

        String CREATE_SONG_TABLE = "CREATE TABLE " + TABLE_SONGS + "("
                + KEY_SONG_ID + " INTEGER PRIMARY KEY autoincrement," + KEY_SONGNAME + " TEXT,"+ KEY_SONGID + " INTEGER" +")";
        db.execSQL(CREATE_SONG_TABLE);

        String CREATE_PLAYLIST_TABLE = "CREATE TABLE " + TABLE_PLAYLIST + "("
                + KEY_PLAYLIST_ID + " INTEGER PRIMARY KEY autoincrement," + KEY_PLAYLIST_DATE + " TEXT,"+ KEY_PLAYLIST_SVRID + " TEXT,"
                + KEY_PLAYLIST_STARTTIME + " TEXT," + KEY_PLAYLIST_ENDTIME + " TEXT," +  KEY_PLAYLIST_TYPE + " TEXT," +  KEY_PLAYLIST_DURATION + " TEXT," +
                KEY_PLAYLIST_SONGNAME + " TEXT," + KEY_PLAYLIST_PATH + " TEXT" +")";
        db.execSQL(CREATE_PLAYLIST_TABLE);

        String CREATE_STATS_TABLE = "CREATE TABLE " + TABLE_STATS + "("
                + KEY_STATS_ID + " INTEGER PRIMARY KEY autoincrement," + KEY_STATS_NAME + " TEXT,"+ KEY_STATS_VALUE + " TEXT" +")";
        db.execSQL(CREATE_STATS_TABLE);


    }

//First tim einsert

    public void firsttime(){

        Add_StatsRecord(new Datastats("config", "notavailable"));
        Add_StatsRecord(new Datastats("playlist","notavailable"));
        Add_StatsRecord(new Datastats("songlist","notavailable"));
        Add_StatsRecord(new Datastats("accountid","notavailable"));
        Add_StatsRecord(new Datastats("congig","notavailable"));
        Add_StatsRecord(new Datastats("busname","notavailable"));
        Add_StatsRecord(new Datastats("busroute","notavailable"));
        Add_StatsRecord(new Datastats("congig","notavailable"));
        Add_StatsRecord(new Datastats("duration","notavailable"));
        Add_StatsRecord(new Datastats("mtd","notavailable"));
        Add_StatsRecord(new Datastats("ytd","notavailable"));
    }


    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECORDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SONGS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new Record
    public void Add_Record(Datarecord datarecord) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DATE, String.valueOf(datarecord.getDate())); // Record Date
        values.put(KEY_TIME, String.valueOf(datarecord.getTime())); // Record Time
        values.put(KEY_TYPE, datarecord.getType()); // Record Type
        values.put(KEY_STATUS, datarecord.getStatus()); // Record Status
        values.put(KEY_EVENTID, datarecord.getEventid()); // Record Eventid
        values.put(KEY_DURATION, String.valueOf(datarecord.getDuration())); // Record Duration
        values.put(KEY_MTD, String.valueOf(datarecord.getMtd())); // Record MTD
        values.put(KEY_YTD, String.valueOf(datarecord.getYtd())); // Record YTD
        // Inserting Row
        db.insert(TABLE_RECORDS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single Record
    public Datarecord Get_Record(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_RECORDS, new String[] { KEY_ID,
                        KEY_DATE, KEY_TIME, KEY_TYPE,KEY_STATUS,KEY_EVENTID,
                        KEY_DURATION,KEY_MTD,KEY_YTD }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Datarecord datarecord = new Datarecord(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),cursor.getString(4),
                cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8));
        // return contact
        cursor.close();
        db.close();

        return datarecord;
    }

    // Getting All Record
    public ArrayList<Datarecord> Get_Records() {
        try {
            datarecords.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_RECORDS;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Datarecord datarecord = new Datarecord();
                    datarecord.setID(Integer.parseInt(cursor.getString(0)));
                    datarecord.setDate(Date.valueOf(cursor.getString(1)));
                    datarecord.setTime(Time.valueOf(cursor.getString(2)));
                    datarecord.setType(cursor.getString(3));
                    datarecord.setStatus(cursor.getString(4));
                    datarecord.setEventid(cursor.getString(5));
                    datarecord.setDuration(Time.valueOf(cursor.getString(6)));
                    datarecord.setMtd(Time.valueOf(cursor.getString(7)));
                    datarecord.setYtd(Time.valueOf(cursor.getString(8)));
                    // Adding contact to list
                    datarecords.add(datarecord);
                } while (cursor.moveToNext());
            }

            // return contact list
            cursor.close();
            db.close();
            return datarecords;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("all_contact", "" + e);
        }

        return datarecords;
    }

    // Updating single Record
    public int Update_Record(Datarecord datarecord) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, String.valueOf(datarecord.getDate()));
        values.put(KEY_TIME, String.valueOf(datarecord.getTime()));
        values.put(KEY_TYPE, datarecord.getType());
        values.put(KEY_STATUS, datarecord.getStatus());
        values.put(KEY_EVENTID, datarecord.getEventid());
        values.put(KEY_DURATION, String.valueOf(datarecord.getDuration()));
        values.put(KEY_MTD, String.valueOf(datarecord.getMtd()));
        values.put(KEY_YTD, String.valueOf(datarecord.getYtd()));


        // updating row
        return db.update(TABLE_RECORDS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(datarecord.getID()) });
    }

    // Deleting single contact
    public void Delete_Record(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECORDS, KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    // Getting contacts Count
    public int Get_Recordcount() {

        String countQuery = "SELECT  * FROM " + TABLE_RECORDS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public void getcolumns(){

        SQLiteDatabase mDataBase = getReadableDatabase();
        Cursor dbCursor = mDataBase.query(TABLE_RECORDS, null, null, null, null, null, null);
        String[] columnNames = dbCursor.getColumnNames();
        for(int i=0; i< columnNames.length; i++){

            Log.e("CHECKMSG","Column names "+i+ " "+columnNames[i]);
        }
    }


    public void Add_SongRecord(Datasong datasong) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_SONGID, datasong.getSongID()); // Record Date
        values.put(KEY_SONGNAME, datasong.getSongName()); // Record Time

        // Inserting Row
        db.insert(TABLE_SONGS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single Record
    public Datasong Get_SongRecord(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SONGS, new String[] { KEY_SONG_ID,
                        KEY_SONGNAME,KEY_SONGID }, KEY_SONGID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Datasong datasong = new Datasong(Integer.parseInt(cursor.getString(0)),
                Integer.parseInt(cursor.getString(1)), cursor.getString(2));
        // return contact
        cursor.close();
        db.close();

        return datasong;
    }
    // Getting All Record
    public ArrayList<Datasong> Get_SongRecords() {
        try {
            datasongs.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_SONGS;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Datasong datasong = new Datasong();
                    datasong.setID(Integer.parseInt(cursor.getString(0)));
                    datasong.setSongName(cursor.getString(1));
                    datasong.setSongID(Integer.valueOf(cursor.getString(2)));
                    // Adding contact to list
                    datasongs.add(datasong);
                } while (cursor.moveToNext());
            }

            // return contact list
            cursor.close();
            db.close();
            return datasongs;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("all_contact", "" + e);
        }

        return datasongs;
    }

    // Getting contacts Count
    public int Get_SongRecordcount() {

        String countQuery = "SELECT  * FROM " + TABLE_SONGS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }


    // Deleting single contact
    public void Delete_SongRecord(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SONGS, KEY_SONG_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    // Adding new Record
    public void Add_PlaylistRecord(Dataplaylist dataplaylist) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PLAYLIST_DATE, String.valueOf(dataplaylist.getDate())); // Record Date
        //values.put(KEY_PLAYLIST_ID, Integer.valueOf(dataplaylist.getplaylistID())); // Record Time
        values.put(KEY_PLAYLIST_TYPE, dataplaylist.getType()); // Record Type
        values.put(KEY_PLAYLIST_STARTTIME, dataplaylist.getStarttime()); // Record Status
        values.put(KEY_PLAYLIST_ENDTIME, dataplaylist.getEndtime()); // Record Eventid
        values.put(KEY_PLAYLIST_DURATION, String.valueOf(dataplaylist.getDuration())); // Record Duration
        values.put(KEY_PLAYLIST_SVRID, String.valueOf(dataplaylist.getSvrid())); // Record MTD
        values.put(KEY_PLAYLIST_SONGNAME, String.valueOf(dataplaylist.getSongname())); // Record YTD
        values.put(KEY_PLAYLIST_PATH, String.valueOf(dataplaylist.getPath())); // Record YTD
        // Inserting Row
        db.insert(TABLE_PLAYLIST, null, values);
        db.close(); // Closing database connection
    }

    // Getting single Record
    public Dataplaylist Get_PlaylistRecord(Time time) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PLAYLIST, new String[] { KEY_PLAYLIST_ID,
                        KEY_PLAYLIST_DATE, KEY_PLAYLIST_TYPE, KEY_PLAYLIST_STARTTIME,KEY_PLAYLIST_ENDTIME,KEY_PLAYLIST_DURATION,
                        KEY_PLAYLIST_SVRID,KEY_PLAYLIST_SONGNAME,KEY_PLAYLIST_PATH }, KEY_PLAYLIST_STARTTIME + "=?",
                new String[] { String.valueOf(time) }, null, null, null, String.valueOf(1));
        if (cursor != null)
            cursor.moveToFirst();

        Dataplaylist dataplaylist = new Dataplaylist(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),cursor.getString(4),
                cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8));
        // return contact
        cursor.close();
        db.close();

        return dataplaylist;
    }

    // Getting All Record
    public ArrayList<Dataplaylist> Get_PlaylistRecords() {
        try {
            dataplaylists.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_PLAYLIST;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Dataplaylist dataplaylist = new Dataplaylist();
                    dataplaylist.setID(Integer.parseInt(cursor.getString(0)));
                    dataplaylist.setDate(String.valueOf(cursor.getString(1)));
                    dataplaylist.setStarttime(cursor.getString(2));
                    dataplaylist.setEndtime(cursor.getString(3));
                    dataplaylist.setDuration(cursor.getString(4));
                    dataplaylist.setType(cursor.getString(5));
                    dataplaylist.setSongname(cursor.getString(6));
                    dataplaylist.setPath(cursor.getString(7));
                    dataplaylist.set_svrid(cursor.getString(8));
                    // Adding contact to list
                    dataplaylists.add(dataplaylist);
                } while (cursor.moveToNext());
            }

            // return contact list
            cursor.close();
            db.close();
            return dataplaylists;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("all_contact", "" + e);
        }

        return dataplaylists;
    }

    // Updating single Record
    public int Update_PlaylistRecord(Dataplaylist dataplaylist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PLAYLIST_DATE, String.valueOf(dataplaylist.getDate())); // Record Date
        //values.put(KEY_PLAYLIST_ID, Integer.valueOf(dataplaylist.getplaylistID())); // Record Time
        values.put(KEY_PLAYLIST_TYPE, dataplaylist.getType()); // Record Type
        values.put(KEY_PLAYLIST_STARTTIME, dataplaylist.getStarttime()); // Record Status
        values.put(KEY_PLAYLIST_ENDTIME, dataplaylist.getEndtime()); // Record Eventid
        values.put(KEY_PLAYLIST_DURATION, String.valueOf(dataplaylist.getDuration())); // Record Duration
        values.put(KEY_PLAYLIST_SVRID, String.valueOf(dataplaylist.getSvrid())); // Record MTD
        values.put(KEY_PLAYLIST_SONGNAME, String.valueOf(dataplaylist.getSongname())); // Record YTD
        values.put(KEY_PLAYLIST_PATH, String.valueOf(dataplaylist.getPath()));


        // updating row
        return db.update(TABLE_PLAYLIST, values, KEY_PLAYLIST_ID + " = ?",
                new String[] { String.valueOf(dataplaylist.getID()) });
    }

    // Deleting single contact
    public void Delete_PlaylistRecord(String  date) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PLAYLIST, KEY_PLAYLIST_DATE + " = ?",
                new String[]{date});
        db.close();
    }

    // Adding new Record
    public void Add_StatsRecord(Datastats datastats) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_STATS_NAME, datastats.getStatname()); // Record Date
        values.put(KEY_STATS_VALUE, datastats.getStatvalue()); // Record Type

        // Inserting Row
        db.insert(TABLE_STATS, null, values);
        db.close(); // Closing database connection
    }

    // Deleting single contact
    public void Delete_StatsRecord(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_STATS, KEY_STATS_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    // Updating single Record
    public int Update_StatsRecord(Datastats datastats) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(KEY_STATS_NAME, String.valueOf(datastats.getStatname())); // Record Date
        //values.put(KEY_PLAYLIST_ID, Integer.valueOf(dataplaylist.getplaylistID())); // Record Time
        values.put(KEY_STATS_VALUE, datastats.getStatvalue()); // Record Type


        // updating row
        return db.update(TABLE_STATS, values, KEY_STATS_NAME + " = ?",
                new String[] { String.valueOf(datastats.getStatname()) });
    }

    // Getting single Record
    public Datastats Get_StatRecord(String name) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_STATS, new String[]{KEY_STATS_ID,
                        KEY_STATS_NAME, KEY_STATS_VALUE}, KEY_STATS_NAME + "=?",
                new String[]{String.valueOf(name)}, null, null, null, String.valueOf(1));
        if (cursor != null)
            cursor.moveToFirst();

        Datastats datastats = new Datastats(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        cursor.close();
        db.close();

        return datastats;
    }


    // Getting All Record
    public ArrayList<Datastats> Get_StatRecords() {
        try {
            datastats.clear();

            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_STATS;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Datastats datastat = new Datastats();
                    datastat.setID(Integer.parseInt(cursor.getString(0)));
                    datastat.setStatname(String.valueOf(cursor.getString(1)));
                    datastat.setStatvalue(cursor.getString(2));

                    // Adding contact to list
                    datastats.add(datastat);
                } while (cursor.moveToNext());
            }

            // return contact list
            cursor.close();
            db.close();
            return datastats;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("all_contact", "" + e);
        }

        return datastats;
    }

}
