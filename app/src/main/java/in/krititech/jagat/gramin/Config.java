package in.krititech.jagat.gramin;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by jagat on 27/03/16.
 */
public class Config extends Activity {

    public String android_id = Build.SERIAL;//.concat("|").concat(Build.FINGERPRINT).concat("|").concat(Build.USER).concat("|").concat(Build.DEVICE).concat("|").concat(Build.ID);   //android.os.Build.SERIAL;
    public String imei_no;
    public String phone_no;
    private static final String LOG_TAG = "CHECKMSG";
    public String  folder_name ;//=   getResources().getString(R.string.folder_name);
    public File configfile ;//= new File(Environment.getExternalStorageDirectory().getPath().concat("/").concat(folder_name), "/config.json");
    private static String server_url = "http://45.58.34.139/graminpracharapp/index.php/graminprachar/alllist/?jsoncallback=?&";
    private String url;
    private TelephonyManager mngr;
    private Context context ;


    //@Override
    protected void onCreate() {
        //super.onCreate();
        try {
        context =getApplicationContext();

    Log.e(LOG_TAG, "Config Class Executed");
    folder_name = getResources().getString(R.string.folder_name);


    configfile = new File(Environment.getExternalStorageDirectory().getPath().concat("/").concat(folder_name), "/config.json");


    mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
    this.imei_no = mngr.getDeviceId();
    this.phone_no = mngr.getLine1Number();
    Log.e(LOG_TAG, "Device IMEI ".concat(imei_no));
    Log.e(LOG_TAG, "Device Phone Number ".concat(phone_no));

    url = this.server_url.concat("deviceid=").concat(this.android_id)
            .concat("&phone=").concat(this.phone_no).concat("&imei=").concat(this.imei_no);

    Log.e(LOG_TAG, "config file url" + url.toString());
}catch (Exception e){
    Log.e(LOG_TAG, "config file exception " + e.toString());
}


    }

    public String readconfig(){

       try {
          // new Downloadconfig().execute(url);
           Log.e(LOG_TAG,"DownloadConfig Url "+url );
       }catch (Exception e){
           Log.e(LOG_TAG,"DownloadConfig Exception"+e.toString() );
       }

        return "123";
    }


        class Downloadconfig extends AsyncTask<String, String, String> {



            @Override
            protected void onPreExecute() {
                // dismiss the dialog after the file was downloaded
                Log.d(LOG_TAG, "Download Started :");

            }
            /**
             * Downloading file in background thread
             * */

            @Override
            protected String doInBackground(String... f_url) {
                int count;
                Log.d(LOG_TAG, "Download progress :Started "+f_url[0]);
                try {
                    URL url = new URL(f_url[0]);
                    String filename = f_url[0].substring(f_url[0].lastIndexOf("/")+1);
                    URLConnection conection = url.openConnection();
                    conection.connect();
                    Log.d(LOG_TAG, "Download progress :"+filename);

                    // this will be useful so that you can show a tipical 0-100%
                    // progress bar
                    int lenghtOfFile = conection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);

                    // Output stream

                    OutputStream output = new FileOutputStream(configfile.toString());

                    byte data[] = new byte[1024];

                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                        // writing data to file
                        output.write(data, 0, count);
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                } catch (Exception e) {
                    Log.e(LOG_TAG, e.getMessage());
                }

                return null;
            }
            /**
             * After completing background task Dismiss the progress dialog
             * **/
            @Override
            protected void onPostExecute(String file_url) {
                // dismiss the dialog after the file was downloaded
                Log.d(LOG_TAG, "Download Completed :");

            }





    }



}
