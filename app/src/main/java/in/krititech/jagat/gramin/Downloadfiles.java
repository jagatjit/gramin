package in.krititech.jagat.gramin;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by jagat on 27/03/16.
 */
class Downloadfiles extends AsyncTask<String, String, String> {
    private static final String LOG_TAG = "CHECKMSG";


    @Override
    protected void onPreExecute() {
        // dismiss the dialog after the file was downloaded
        Log.d(LOG_TAG, "Download Started :");

    }
    /**
     * Downloading file in background thread
     * */

    @Override
    protected String doInBackground(String... f_url) {
        int count;
        Log.d(LOG_TAG, "Download progress :Started "+f_url[0]);
        try {
            URL url = new URL(f_url[0]);
            String filename = f_url[0].substring(f_url[0].lastIndexOf("/")+1);
            URLConnection conection = url.openConnection();
            conection.connect();
            Log.d(LOG_TAG, "Download progress :"+filename);

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream
            File myFile = new File(Environment.getExternalStorageDirectory().getPath(), "/GraminPrachar/".concat(filename));
            OutputStream output = new FileOutputStream(myFile.toString());

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }

        return null;
    }
    /**
     * After completing background task Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String file_url) {
        // dismiss the dialog after the file was downloaded
        Log.d(LOG_TAG, "Download Completed :");

    }


}