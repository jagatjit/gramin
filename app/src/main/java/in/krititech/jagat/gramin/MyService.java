package in.krititech.jagat.gramin;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {


    private static final String LOG_TAG = "CHECKMSG";


    @Override
    public void onCreate() {


        Log.d(LOG_TAG, "Service created!!  On Create!");
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.

        Log.d(LOG_TAG, "Service created!");
        throw new UnsupportedOperationException("Not yet implemented");


    }

    public void MyService() {
        Log.d(LOG_TAG, "Service created!  for My service");
    }

    public class myReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context ctx, Intent intent) {

            Log.d(LOG_TAG, "====Bringging Application to Front====");

            intent = new Intent(getBaseContext(), MainActivity.class);
            if (intent.getAction().equals(Intent.ACTION_USER_PRESENT) || intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                ctx.startService(intent);
            }
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                ctx.stopService(intent);
            }

           /* if(activityVisible == false) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplication().startActivity(intent);
                MainActivity.activityResumed();
            }*/

        }
    }
}
