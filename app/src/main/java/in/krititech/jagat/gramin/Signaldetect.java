package in.krititech.jagat.gramin;

/**
 * Created by jagat on 31/03/16.
 */

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Signaldetect extends Activity {

    static Signaldetect mainApp;

    public static final int DETECT_NONE = 0;
    public static final int DETECT_WHISTLE = 1;
    public static int selectedDetection = DETECT_NONE;

    // detection parameters
    private DetectorThread detectorThread;
    private RecorderThread recorderThread;
    public int numWhistleDetected = 0;
    private String LOG_TAG= "CHECKMSG";

    // views
   // private View mainView, listeningView;
    private Button whistleButton,whistleButtonstop;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("musicg WhistleAPI Demo");

        mainApp = this;

        // set views
        LayoutInflater inflater = LayoutInflater.from(this);
        //mainView = inflater.inflate(R.layout.main, null);
        //listeningView = inflater.inflate(R.layout.listening, null);
        //setContentView(mainView);

        whistleButton = (Button) this.findViewById(R.id.whistleButton);
        whistleButton.setOnClickListener(new ClickEvent());

        whistleButtonstop = (Button) this.findViewById(R.id.whistleButtonstop);
        whistleButtonstop.setOnClickListener(new ClickEventstop());
    }

    public void goHomeView() {
      //  setContentView(mainView);
        if (recorderThread != null) {
            recorderThread.stopRecording();
            recorderThread = null;
        }
        if (detectorThread != null) {

            numWhistleDetected =detectorThread.totalWhistlesDetected;
            detectorThread.stopDetection();
            detectorThread = null;
        }
        selectedDetection = DETECT_NONE;

        Log.e(LOG_TAG, "goHomeView activated" );
    }

    public void goListeningView(){

        selectedDetection = DETECT_WHISTLE;
        recorderThread = new RecorderThread();
        recorderThread.startRecording();
        detectorThread = new DetectorThread(recorderThread);
        detectorThread.start();
        //setContentView(listeningView);

        Log.e(LOG_TAG, "goListeningView activated" );
    }






    class ClickEvent implements OnClickListener {
        public void onClick(View view) {
            if (view == whistleButton) {

                goListeningView();
            }
        }
    }

    class ClickEventstop implements OnClickListener {
        public void onClick(View view) {
            goHomeView();
            Log.e(LOG_TAG, "total Whistle " +numWhistleDetected);

        }
    }

    protected void onDestroy() {
        super.onDestroy();
        android.os.Process.killProcess(android.os.Process.myPid());
    }


}

