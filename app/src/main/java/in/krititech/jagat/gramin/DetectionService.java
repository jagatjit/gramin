package in.krititech.jagat.gramin;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by jagat on 30/03/16.
 */
public class DetectionService extends Service implements
        DetectorThread.OnWhistleListener {
    public String LOG_TAG = "CHECKMSG";

    Handler handler;
    private DetectorThread detectorThread;
    private RecorderThread recorderThread;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        Log.e(LOG_TAG, " Detection App started ");


    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            if (intent != null && intent.getExtras() != null) {

                if (intent.getExtras().containsKey("action")) {
                    Log.e(LOG_TAG, "action : " + intent.getStringExtra("action"));

                    if (intent.getStringExtra("action").equals("start")) {
                        startWhistleDetection();
                    }

                    if (intent.getStringExtra("action").equals("stop")) {
                        stopWhistleDetection();
                        stopSelf();
                    }
                }
            } else {

                startWhistleDetection();
                Log.e(LOG_TAG, "intent is null OR intent.getExtras() is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void startWhistleDetection() {

        try {
            stopWhistleDetection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        recorderThread = new RecorderThread();
        recorderThread.startRecording();
        detectorThread = new DetectorThread(recorderThread);
        detectorThread.setOnWhistleListener(this);
        detectorThread.start();

    }

    private void stopWhistleDetection() {
        if (detectorThread != null) {
            detectorThread.stopDetection();
            detectorThread.setOnWhistleListener(null);
            detectorThread = null;
        }

        if (recorderThread != null) {
            recorderThread.stopRecording();
            recorderThread = null;
        }

    }



    @Override
    public void onWhistle() {
        Log.e(LOG_TAG, "onWhistle()");
    }
}
