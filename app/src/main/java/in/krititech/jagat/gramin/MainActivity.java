package in.krititech.jagat.gramin;

import android.app.Application;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;

import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.provider.Telephony;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.mycompany.myfirstapp.MESSAGE";
    private static final String LOG_TAG = "CHECKMSG";
    private MediaPlayer mMediaPlayer,mMediaplayeralert;
    private boolean mMediaplayerMute = false;
    private String[] mMusicList;
    private MusicIntentReceiver myReceiver;
    private String configtime,songlisttime,playlisttime;

    private String mAudioPathlist;
    private ListAdapter mAdapter;
    private ListView lv;
    private String songPath;
    private Integer songIndex =0;
    private Integer devicesongIndex =0;
    private String[] songs,playlistsongs;
    private String[] mAudioPath,playlistmAudioPath,downloadsongs;
    private String[][] songsarray,devicesongsarray;

    private String songslist;
    private View view;
    private  static int audiomode;
    private boolean isnetconnected = false;
    private Context context ;
    private MediaRecorder mRecorder = null;
    private String android_id = Build.SERIAL;//.concat("|").concat(Build.FINGERPRINT).concat("|").concat(Build.USER).concat("|").concat(Build.DEVICE).concat("|").concat(Build.ID);   //android.os.Build.SERIAL;
    private String imei_no;
    private String phone_no;
    private static String serveraddr ="http://45.58.34.139/graminpracharapp/index.php/graminprachar/";
    private static String server_url = "http://45.58.34.139/graminpracharapp/index.php/graminprachar/alllist/?jsoncallback=?&";
    private static String is_register = "http://45.58.34.139/graminpracharapp/index.php/graminprachar/is_register/?jsoncallback=?&";
    private static String download_url = "http://45.58.34.139/gramminprachar/";
    private static String file_url = server_url.concat("accid=23/playlist.txt");
    private static String songfile_url = server_url.concat("accid=23/songlist.txt");
    private static String songackurl = "http://45.58.34.139/graminpracharapp/index.php/graminprachar/download/?jsoncallback=?&accid=23&songid=2&deviceid=4f977e9549b6d340";
    private static String sdcardpath = Environment.getExternalStorageDirectory().getPath();
    private boolean headsetisConnected = false;
    private boolean playbytime = false;
    private boolean isconfigfile = false;
    private boolean isfolder = false ;
    private boolean isplaylistfile = false ;
    private boolean iswhistleplaying = true ;
    Handler mHandler = new Handler();
    private String folder_name;
    private String configfile;
    private String accountid;
    private  String busname,bustime,busroute,destination,duration,ytd,mtd;
    TextToSpeech t1;
    private Intent intent;
    private PackageManager packageManager;
    private JSONObject playlist;
    private Signaldetect signaldetect;
    public static int NAME,PATH,TYPE,START_TIME,DURATION,SONGID;
    DbHandler dbHandler =  new DbHandler(this);
    //private Config config;
    private TextToSpeech myTTS;
    //status check code
    private int MY_DATA_CHECK_CODE = 0;
    private MyService myService;
    IntentFilter filter;
    Uri otherPath = Uri.parse("android.resource://in.krititech.jagat.gramin/local/noplaylist.mp3");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


//-----------------------------------------Setup check------------------------

        this.NAME = 0;
        this.PATH = 1;
        this.TYPE = 2;
        this.DURATION = 3;
        this.START_TIME =4 ;
        this.SONGID =5;
        context =getApplicationContext();


//---------------------------------------- Check Started ---------------------
        boolean sdcardwritable = isExternalStorageWritable();
        boolean sdcardreadable = isExternalStorageReadable();

        folder_name = getResources().getString(R.string.folder_name);
        displaysetup();
        GetRingermode();
        myReceiver = new MusicIntentReceiver();
        mMediaPlayer = new MediaPlayer();
        mMediaplayeralert = new MediaPlayer();

        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.pref_key), MODE_PRIVATE);
        String defaultvalue = getResources().getString(R.string.pref_key);
        PackageManager packageManager = context.getPackageManager();
        intent = packageManager.getLaunchIntentForPackage("in.krititech.jagat.gramin");

        TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        this.imei_no= mngr.getDeviceId();
        this.phone_no = mngr.getLine1Number();
        getAudioList();
        signaldetect = new Signaldetect();
        myService = new MyService();
        myService.MyService();


       /* String message = " \nWritable:".concat(String.valueOf(sdcardwritable))
                .concat(" \nReadable:").concat(String.valueOf(sdcardreadable))
                .concat("\nFile: ").concat(String.valueOf(file));
        TextView textView = (TextView) findViewById(R.id.textview);
        textView.setText(message);
*/
                /*LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gpsenabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(!gpsenabled){

            // Create a dialog here that requests the user to enable GPS, and use an intent

        }
*/

        //---------------------------------
        //check for TTS data
       /* Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);

            if(myTTS.isLanguageAvailable(Locale.US)==TextToSpeech.LANG_AVAILABLE)
                myTTS.setLanguage(Locale.US);
        //myTTS.speak("Hello How are you", TextToSpeech.QUEUE_FLUSH, null,null);

*/
        //----------------------------

        try {
            Readstats(null);
        } catch (Exception e) {

            dbHandler.firsttime();
            Log.e(LOG_TAG, "Read Stats  Exception :" + e.toString());
        }






    }

    public void Refreshconfig( View view) throws IOException {

        //-----------------------------Turn Internet on off ----------------------------
        isnetconnected= Checkinternetconnectivity();
        if(!isnetconnected) {
            try {
                setMobileDataEnabled(context, true);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
        //canToggleGPS();
        //turnGPSOn();
       //turnGPSOff();



        //---------------------------------------------------------------------------


        GetWhistle();
        isfolder = checkfolder();
        isnetconnected= Checkinternetconnectivity();
        headsetisConnected = detectheaset(context, intent);
        isconfigfile = checkconfig();
        if(!isnetconnected && !isconfigfile){

            Log.e(LOG_TAG,"Please connect to internet");
        }
        if(isconfigfile){

            try {
                readconfig();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

             displaysetup();

          try {
                downloadsongs();
            } catch (IOException e) {
                e.printStackTrace();
            }


            isplaylistfile = Checkplaylistfile();
            if(isplaylistfile){

                Log.e(LOG_TAG,"YES CHECKLIST FILE");
                try {
                    readplaylist();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                NextSong(null);
            }

           // displayplaylist();
        }



/*
        isnetconnected= Checkinternetconnectivity();
        headsetisConnected = detectheaset(context, intent);
        isconfigfile = checkconfig();
        if(!isnetconnected && !isconfigfile){

            Log.e(LOG_TAG,"Please connect to internet");
        }
        if(isconfigfile){

            try {
                readconfig();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            displaysetup();

            try {
                downloadsongs();
            } catch (IOException e) {
                e.printStackTrace();
            }

            isplaylistfile = Checkplaylistfile();
            if(isplaylistfile){
                try {
                    readplaylist();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(LOG_TAG, "Playlist Read EXception " + e.toString());
                    File myFile = new File(sdcardpath+"/"+folder_name+"/playlist.txt");
                    if(myFile.exists())
                    myFile.delete ();
                    isplaylistfile=Checkplaylistfile();
                }
                NextSong(null);
                Playlistdisplay();
            }

        }
*/


    }

    public void Readstats( View view) throws IOException{

        Datastats datastats = dbHandler.Get_StatRecord("config");
        this.configtime = datastats.getStatvalue();


        datastats = dbHandler.Get_StatRecord("songlist");
        this.songlisttime = datastats.getStatvalue();

        datastats = dbHandler.Get_StatRecord("playlist");
        this.playlisttime = datastats.getStatvalue();

        if(this.configtime.equals("notavailable")){
            Log.e(LOG_TAG,"NOT AVAILABLE");
        }

        try {
            Refreshconfig(null);
        } catch (IOException e) {

            Log.e(LOG_TAG,"Refresh Exception :"+e.toString());
            e.printStackTrace();
        }catch (Exception e) {

            Log.e(LOG_TAG,"Refresh Exception :"+e.toString());
            e.printStackTrace();
        }


    }

    private void displayplaylist() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_1,playlistsongs );
        lv = (ListView) findViewById(R.id.listView1);
        lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lv.setSelection(2);
        lv.setSelected(true);
        lv.setItemChecked(2, true);
        lv.setAdapter(arrayAdapter);
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible = true;

    private java.sql.Date getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return java.sql.Date.valueOf(dateFormat.format(date));
    }

    private Time getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return Time.valueOf(dateFormat.format(date));
    }

    @Override public void onResume() {
        if(filter==null) {
            filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
            registerReceiver(myReceiver, filter);
        }
        super.onResume();
        MainActivity.activityResumed();
    }

    @Override public void onPause() {
        //unregisterReceiver(myReceiver);
        super.onPause();
        MainActivity.activityPaused();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();

        Log.e(LOG_TAG,"DESTROYED");
    }


    /** Called when the user clicks the Send button */



    public void NextSong(View view) {

// Do something in response to button
// audio identification starts in Play Song  and ends here
//if the count of whistles are less  the songs to go to mute mode


        String status = "complete";

        java.sql.Date date = getDate();
        Time time = getTime();
        String type = "song";
        String statname = "";
        String statvalue="";

        String eventid = "23";
        Time duration = getTime();
        Time mtd = getTime();
        Time ytd = getTime();
        Integer id = 0;
        if (mMediaplayerMute) {
            status = "incomplete";
        }
        Log.d(LOG_TAG, "DB record  : " + date + " " + time + " " + type + " " + status + " " + eventid + " " + duration + " " + mtd + " " + ytd);
        String downloadpath = "http://45.58.34.139/graminpracharapp/index.php/graminprachar/updatedb/?jsoncallback=?&position=1.33&" +
                "songid=" + this.songIndex + "&time=" + time + "&formatted_address=&accid=" + this.accountid + "&playlist_id=704&frequency=" + status + "/test.test";



         //dbHandler.getcolumns();
        dbHandler.Add_Record(new Datarecord(date, time, type, status, eventid, duration, mtd, ytd));


        Integer count = dbHandler.Get_Recordcount();
 /*       new UpdateStatus().execute(downloadpath.replace(" ", "%20"));

        ArrayList<Datarecord> record_array_from_db = dbHandler.Get_Records();

            for(int i=record_array_from_db.size()-1;i < record_array_from_db.size(); i++){

                id = record_array_from_db.get(i).getID();
                date = record_array_from_db.get(i).getDate();
                time = record_array_from_db.get(i).getTime();
                status = record_array_from_db.get(i).getStatus();
                eventid = record_array_from_db.get(i).getEventid();
                duration = record_array_from_db.get(i).getDuration();
                mtd = record_array_from_db.get(i).getMtd();
                ytd = record_array_from_db.get(i).getYtd();

                Log.d(LOG_TAG, "DB record from DB  : "+count+" "+id +" "+date+" "+time+" "+type+" "+status+" "+eventid+" "+duration+" "+mtd+" "+ytd);
            }
*/
 /*       ArrayList<Datastats> statrecord_array_from_db = dbHandler.Get_StatRecords();

        for(int i=0;i < statrecord_array_from_db.size(); i++){

            id = statrecord_array_from_db.get(i).getID();
            statname = statrecord_array_from_db.get(i).getStatname();
            statvalue = statrecord_array_from_db.get(i).getStatvalue();


            Log.d(LOG_TAG, "DB Stat record from DB  : "+id +" "+statname+" "+statvalue);
        }
*/

        this.songIndex += 1;
        if(songsarray.length>2){
        try {

            DateFormat formatter = new SimpleDateFormat("HH:mm");
            Time timeValue;
            if(playbytime) {
                timeValue = new Time(formatter.parse(songsarray[this.songIndex][START_TIME]).getTime());
                for(int i=this.songIndex; i < songsarray.length && time.after(timeValue) ; i++) {
                    this.songIndex += 1;
                    try {
                         timeValue = new Time(formatter.parse(songsarray[this.songIndex][START_TIME]).getTime());
                        if (time.after(timeValue)) {
                            Log.e(LOG_TAG, "After SONG INDEX " + time.toString() + " " + songsarray[this.songIndex][START_TIME]);
                            //this.songIndex += 5;
                        } else if (time.before(timeValue)) {
                            Log.e(LOG_TAG, "Before SONG INDEX " + time.toString() + " " + songsarray[this.songIndex][START_TIME]);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            Log.e(LOG_TAG, "EXCEPTION HERE SONG INDEX " + songsarray[this.songIndex][START_TIME]);


            if (this.songIndex > playlistsongs.length) {
                this.songIndex = 0;
            } else {

                //Log.e(LOG_TAG, "EXCEPTION HERE SONG INDEX " + this.songIndex);
                //Log.d(LOG_TAG, "Song Type detected : " + songsarray[this.songIndex][TYPE] + " " + songsarray[this.songIndex][NAME]);


                if (CheckSongExists()) {

                    try {
                        Log.d(LOG_TAG, "Song index: " + this.songIndex);
                        playSong(this.songIndex);
                    } catch (Exception e) {
                        Log.d(LOG_TAG, "Activity status Exception : " + e.toString());
                        e.printStackTrace();
                    }

                }
            }
        } catch (Exception e) {

            Log.e(LOG_TAG, "Array Null Pointer Exception " + e.toString());
        }
    }
    }


    private void Checkwistle(Integer songIndex) throws IllegalArgumentException,
            IllegalStateException, IOException {




        mMediaPlayer.reset();
        mMediaPlayer.setDataSource(sdcardpath + "/" + folder_name + "/sweetwhistl.mp3");
        mMediaPlayer.prepare();
        mMediaPlayer.start();
        signaldetect.numWhistleDetected =0;
        signaldetect.goListeningView();
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                signaldetect.goHomeView();
                Log.d(LOG_TAG, "Total Whistle detected : " + signaldetect.numWhistleDetected);
                if(signaldetect.numWhistleDetected <1)
                    stopTimer();
                else
                startTimer();

                NextSong(null);
            }
        });
    }

    private void alertspeaker() throws IllegalArgumentException,
            IllegalStateException, IOException {



        mMediaPlayer.setVolume(0,0);
        mMediaplayeralert.reset();
        mMediaplayeralert.setDataSource(sdcardpath + "/" + folder_name + "/connectspeaker.mp3");
        mMediaplayeralert.prepare();
        mMediaplayeralert.start();
        mMediaplayeralert.setVolume(1,1);
        mMediaplayeralert.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

              //  mMediaplayeralert.setVolume(0,0);
              //  mMediaPlayer.setVolume(1,1);

                Log.d(LOG_TAG, "Speaker Alert detected : ");
                mMediaPlayer.setVolume(0,0);

            }
        });
    }



    public void PreviousSong(View view){

// Do something in response to button

        if(this.songIndex<=0)
        this.songIndex=0;
        else
        this.songIndex-=1;

        try {
            playSong(this.songIndex);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(LOG_TAG, this.mAudioPath[this.songIndex]);

        //  ---------------------------Send Application to Background -----------------------

        boolean sentAppToBackground = moveTaskToBack(true);

        if(!sentAppToBackground){
            Intent i = new Intent();
            i.setAction(Intent.ACTION_MAIN);
            i.addCategory(Intent.CATEGORY_HOME);
            this.startActivity(i);
        }
    //-------------------------------------End ----------------------------
    }


    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    public File getAlbumStorageDir(String albumName) throws IOException {
        // Get the directory for the user's public pictures directory.
        File myDir = new File(Environment.getExternalStorageDirectory().getPath(), albumName);

        if (!myDir.exists()) {
            Log.e(LOG_TAG, folder_name.toString()+" Folder doesnot not exist");

            if (!myDir.mkdirs()) {
                Log.e(LOG_TAG,  folder_name.toString()+" Folder Cannot Create");

            }else Log.e(LOG_TAG,  folder_name.toString()+" Folder created");

        }else
            Log.e(LOG_TAG,  folder_name.toString()+ "Folder Exists".concat(myDir.toString()));


        if (myDir.exists()) {

            String fname = "playlist.txt";
            String fulldata = "";
            File file = new File(myDir, fname);
            if (file.exists()) {
                //file.delete ();
                BufferedReader br = null;

                try {
                    br = new BufferedReader(new FileReader(file));
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                String line = "";

                while ((line = br.readLine()) != null) {
                    //Do something here  Read Playlist.txt
                    fulldata = fulldata.concat(line);
                    //Log.e(LOG_TAG, "Line Line".concat(line));

                }
                //Log.e(LOG_TAG, "____________________________________________________________\n".concat(fulldata));

               // fulldata = "?({\t\t\t\t\"items\": [{\"playlist\":{\"1\":{\"song_name\":\"\\/GRAMINPRACHAR\\/10706152.mp3\",\"song\":\"UPLOAD\\/10706152.mp3\"}}}], })";

                fulldata = fulldata.substring(2,(fulldata.length()-4)).concat("}");
                try {

                    JSONObject jsonObj  = new JSONObject(fulldata);
                    JSONObject jsonObj2 = jsonObj.optJSONArray("items").getJSONObject(0).getJSONObject("playlist");
                    JSONArray jsonarray = jsonObj2.names();
                    Comparator c = null;
                    //jsonarray=sort(jsonarray,c);
                    for (int i=1;   i< 2 /*jsonObj2.length()+1*/; i++) {

                        //JSONObject jsonObj3 = jsonObj2.getJSONObject(jsonarray.getString(i));
                        JSONObject jsonObj3 = jsonObj2.getJSONObject(String.valueOf(i));
                        String name = jsonObj3.getString("song_name");
                        String path = sdcardpath.concat(name);
                        String downloadpath = download_url+"UPLOAD/".concat(jsonObj3.getString("song_name"))+"/"+
                                name.substring(name.lastIndexOf("/") + 1);
                        File checkfile = new File(path);
                        if(!checkfile.exists()){
                            new DownloadFileFromURL().execute(downloadpath);
                        }
                        //JSONArray jsonArray2 = jsonObj2.getJSONArray("playlist");
                        Log.e(LOG_TAG, "Song read errrrrr" + String.valueOf(i) + " "+name);
                        // allNames.add(name);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(LOG_TAG,"Song read error "+e.toString());
                }

                //Log.e(LOG_TAG, "Jason  Line ".concat(fulldata.substring(600)));

                } else {

                    // -------------------Reading from internet-------------------------



                        new DownloadFileFromURL().execute(file_url);
                        Log.d(LOG_TAG, "Getting Playlist from Net");



                    //------------------------------------------------------------------

            }
        }
        return myDir;
    }

    /*public File getAlbumStorageDirPrivate(Context context, String albumName) {
        // Get the directory for the app's private pictures directory.
        File file = new File(context.getExternalFilesDir(
                Environment.DIRECTORY_MUSIC), albumName);
        return file;
    }*/

    public static JSONArray sort(JSONArray array, Comparator c){
        List asList = new ArrayList(array.length());
        for (int i=0; i<array.length(); i++){
            asList.add( array.opt(i));
        }
        Collections.sort(asList, c);
        JSONArray  res = new JSONArray();
        for (Object o : asList){
            res.put(o);
        }
        return res;
    }





    private String[] getAudioList() {
        final Cursor mCursor = getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Media.DISPLAY_NAME, MediaStore.Audio.Media.DATA,MediaStore.Audio.Media.DURATION,
                        MediaStore.Audio.Media.IS_MUSIC }, null, null,
                "LOWER(" + MediaStore.Audio.Media.TITLE + ") ASC");

        int count = mCursor.getCount();
        devicesongsarray = new String[count][4];

        this.songslist = "";
        this.mAudioPathlist = "";
        this.songs = new String[count];
        this.mAudioPath = new String[count];
        int i = 0;
        if (mCursor.moveToFirst()) {
            do {
                this.songs[i] = mCursor.getString(mCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME));
                this.mAudioPath[i] = mCursor.getString(mCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA));
                this.songslist = this.songslist.concat(this.songs[i]).concat("|");
                this.mAudioPathlist = this.mAudioPathlist.concat(this.mAudioPath[i]).concat("|");
                devicesongsarray[i][NAME] = mCursor.getString(mCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME));
                devicesongsarray[i][PATH] =mCursor.getString(mCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA));
                devicesongsarray[i][TYPE]= mCursor.getString(mCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.IS_MUSIC));
                devicesongsarray[i][DURATION] = mCursor.getString(mCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION));

               // Log.d(LOG_TAG, mAudioPath[i]);
                i++;
            } while (mCursor.moveToNext());
        }

        mCursor.close();
        //Log.d(LOG_TAG, "Path list".concat(this.mAudioPathlist));
        //Log.d(LOG_TAG, "Audio List".concat(this.songslist));
        return null;
    }



    private void playSongwithpath(String path,Integer seekto ) throws IllegalArgumentException,
            IllegalStateException, IOException {

        Log.d(LOG_TAG, "playSong :: " + path);

     mMediaPlayer.reset();
     mMediaPlayer.setDataSource(path);
//mMediaPlayer.setLooping(true);
     mMediaPlayer.prepare();
     mMediaPlayer.seekTo(seekto);
     mMediaPlayer.start();
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                NextSong(null); //4346 7860
            }
        });
    }

private Integer MintoSec(String duration){

    Integer durationinsec,minuites,seconds;
    durationinsec=0;
    try {

        if (duration.indexOf(":") >= 0) {
            minuites = Integer.valueOf(duration.substring(0, duration.lastIndexOf(":"))) * 60;
            seconds = Integer.valueOf(duration.substring(duration.lastIndexOf(":")+1));
            durationinsec = (minuites + seconds) * 1000;
        } else
            durationinsec = Integer.valueOf(duration.substring(duration.lastIndexOf(":")+1)) * 1000;
    }catch (Exception e){
        Log.e(LOG_TAG,"Integer Exception "+e.toString());
    }
    return durationinsec;
}
    private boolean CheckSongExists(){
        File myFile = new File(songsarray[songIndex][PATH]);
        if(myFile.exists()){
            return true;
        }else {

            for(int i=devicesongIndex; i < devicesongsarray.length; i++){
                devicesongIndex++;
                if(devicesongIndex >=devicesongsarray.length)
                    devicesongIndex=0;

                Log.e(LOG_TAG,"DEVICE SONGS :"+devicesongsarray[i][TYPE]+" "+devicesongsarray[i][DURATION]+" "+MintoSec(songsarray[songIndex][DURATION])+" "+devicesongsarray[i][PATH]);
                Integer songduration = MintoSec(songsarray[songIndex][DURATION]);
                Integer devicesongduration = Integer.valueOf(devicesongsarray[i][DURATION]);
               if( Integer.valueOf(devicesongsarray[i][TYPE]).equals(1) && devicesongduration > songduration) {
                   Integer seekto = devicesongduration - songduration ;
                   try {
                       playSongwithpath(devicesongsarray[i][PATH],seekto);
                   } catch (IOException e) {
                       e.printStackTrace();

                   }
                   i = devicesongsarray.length;
               }
            }
           return  false;
        }
    }


    private void playSong(final Integer songIndex) throws IllegalArgumentException,
            IllegalStateException, IOException {


        Log.d(LOG_TAG, "playSong :: " + " " + songIndex + " "  + " " + songsarray[songIndex][TYPE] + " " +
                songsarray[songIndex][START_TIME] + " " + songsarray[songIndex][PATH]);


        mMediaPlayer.reset();
        mMediaPlayer.setDataSource(songsarray[songIndex][PATH]);
        //mMediaPlayer.setLooping(true);
        mMediaPlayer.prepare();
        mMediaPlayer.start();

        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                String songtype = songsarray[songIndex][TYPE];
                Log.e(LOG_TAG,"Song Completed  for :"+songtype+":");
                if(!songtype.equals("mp3")){
                    Log.e(LOG_TAG,"Playing Whistle  for :"+songsarray[songIndex][TYPE]+":");
                    try {
                        Checkwistle(songIndex);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(LOG_TAG, "Whistle  for :" + songsarray[songIndex][TYPE] + ":");
                    }
                }else{

                NextSong(null);
                }
            }
        });
    }




    private void playpauseSong() throws IllegalArgumentException,
            IllegalStateException, IOException {


      /*  if(mMediaPlayer.isPlaying())
            mMediaPlayer.pause();
        else
            mMediaPlayer.start();
        */
        if(mMediaplayerMute==true){
            mMediaplayerMute = false;
            mMediaPlayer.setVolume(1,1);
        }else{
            mMediaplayerMute = true;
            mMediaPlayer.setVolume(0, 0);

        }

    }

    private void GetWhistle(){

        File whilstle = new File(sdcardpath+"/"+folder_name+"sweetwhistl.mp3");
        if(!whilstle.exists()){
            new DownloadManager(this).execute(download_url+"/UPLOAD/sweetwhistl.mp3","sweetwhistl.mp3","3");
        }
        File speaker = new File(sdcardpath+"/"+folder_name+"connectspeaker.mp3");
        if(!speaker.exists()){
            new DownloadManager(this).execute(download_url+"/UPLOAD/connectspeaker.mp3","connectspeaker.mp3","3");
        }
    }

private void GetRingermode(){


    AudioManager am;
    am= (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);

    audiomode = am.getRingerMode();
    Log.d(LOG_TAG, "Current Ringer Mode :".concat(String.valueOf(audiomode)));
}


    private boolean Checkinternetconnectivity() {


        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null) {
                for (int i = 0; i < 2; i++) {

                    if (info.getState() == NetworkInfo.State.CONNECTED) {
                        isnetconnected = true;
                        Log.d(LOG_TAG, "1st Current Network Mode :".concat(String.valueOf(isnetconnected)));
                    }
                }
            }else {
                isnetconnected = false;
                Log.d(LOG_TAG, "2nd Current Network Mode :".concat(String.valueOf(isnetconnected)));
            }
        }else {
            isnetconnected = false;
            Log.d(LOG_TAG, "3rd Current Network Mode :".concat(String.valueOf(isnetconnected)));
        }


return isnetconnected;

    }

    private void registerdevice(){

        String url2 = this.is_register.concat("deviceid=").concat(this.android_id)
                .concat("&phone=").concat(this.phone_no).concat("&imei=").concat(this.imei_no);

        try {

            URL url1 = new URL(url2);
            new DownloadManager(this).execute(url2, "config.json","0");
            Log.d(LOG_TAG, "URL Register :".concat(url2));

        } catch (IOException e) {
            e.printStackTrace();
        }


       // return null;
    }

    private void Uploadsonglist() {

        String url = server_url.concat("deviceid=").concat(this.android_id)
                .concat("&allitem=").concat(this.songslist).concat("&songfullph=").concat(this.mAudioPathlist);
        try {
            URL url1 = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.d(LOG_TAG, "URL Exception :".concat(e.toString()));
        }

    }
//--------------------------------------Sound Meter-------------------------------



    public void Soundmeterstart( View view) throws IOException {

        if (mRecorder == null) {
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile("/dev/null");
            mRecorder.prepare();
            mRecorder.start();
            Log.d(LOG_TAG, "Sound Meter Started :");
        }
    }

    public void Soundmeterstop(View view) {
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
            Log.d(LOG_TAG, "Sound Meter stopped :");
        }
        NextSong(null);

    }

    public double SoundmetergetAmplitude(View view) {

        try {
            playpauseSong();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(LOG_TAG, "Sound Meter data : 0"+e.toString());
        }

        if (mRecorder != null) {

            Log.d(LOG_TAG, "Sound Meter data : ".concat(String.valueOf(mRecorder.getMaxAmplitude())));
            return  mRecorder.getMaxAmplitude();
        }
        else {

            Log.d(LOG_TAG, "Sound Meter data : 0");
            return 0;
        }


    }
//-------------------------------------------------------Class Update songs-------------------------------------------------------
class UpdateStatus extends AsyncTask<String, String, String> {



    @Override
    protected void onPreExecute() {
        // dismiss the dialog after the file was downloaded
        Log.d(LOG_TAG, "Update Started :");

    }
    /**
     * Downloading file in background thread
     * */

    @Override
    protected String doInBackground(String... f_url) {
        int count;
        try {
            String urllink = f_url[0].substring(0,f_url[0].lastIndexOf("/")).replace(" ", "%20");
            Log.d(LOG_TAG, "Update URL : "+urllink);

            URL url = new URL(urllink);
            String filename = f_url[0].substring(f_url[0].lastIndexOf("/")+1);
            URLConnection conection = url.openConnection();
            conection.connect();
            Log.d(LOG_TAG, "Update filename : "+filename);

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream
        //    File myFile = new File(Environment.getExternalStorageDirectory().getPath(), "/GraminPrachar/".concat(filename));
        //    Log.d(LOG_TAG, "Update Location : "+myFile.toString());
         //   OutputStream output = new FileOutputStream(myFile.toString());

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
               // output.write(data, 0, count);
            }

            // flushing output
        //    output.flush();

            // closing streams
        //    output.close();
            input.close();

        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }

        return null;
    }
    /**
     * After completing background task Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String file_url) {
        // dismiss the dialog after the file was downloaded
        Log.d(LOG_TAG, "Download Completed :");

    }


}
//-------------------------------------------------------class DownloadFileFromURL -----------------------------------------------

    class DownloadFileFromURL extends AsyncTask<String, String, String> {



        @Override
        protected void onPreExecute() {
            // dismiss the dialog after the file was downloaded
            Log.d(LOG_TAG, "Download Started :");

        }
        /**
         * Downloading file in background thread
         * */

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String urllink = f_url[0].substring(0,f_url[0].lastIndexOf("/")).replace(" ", "%20");
                Log.d(LOG_TAG, "Download URL : "+urllink);

                URL url = new URL(urllink);
                String filename = f_url[0].substring(f_url[0].lastIndexOf("/")+1);
                URLConnection conection = url.openConnection();
                conection.connect();
                Log.d(LOG_TAG, "Download filename : "+filename);

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                File myFile = new File(Environment.getExternalStorageDirectory().getPath(), "/GraminPrachar/".concat(filename));
                Log.d(LOG_TAG, "Download Location : "+myFile.toString());

                OutputStream output = new FileOutputStream(myFile.toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                if(filename.equals("statsfile") ){
                    UpdateStats();
                }
                if(filename.equals("songlist.txt") ){
                    downloadsongs();
                }
                if(filename.equals("playlist.txt") ){
                    readplaylist();
                }


            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }

            return null;
        }
        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            Log.d(LOG_TAG, "Download Completed :");

        }


    }

    private void UpdateStats() throws IOException, JSONException {

        String statsfile = sdcardpath + "/".concat(folder_name) + "/config.json";
        File myFile = new File(statsfile);
        BufferedReader br = null;
        String fulldata ="";
        try {
            br = new BufferedReader(new FileReader(myFile));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        String line = "";

        while ((line = br.readLine()) != null) {
            //Do something here  Read Playlist.txt
            fulldata = fulldata.concat(line);

        }


        fulldata = fulldata.substring(2, (fulldata.length() - 4)).concat("}");
        Log.e(LOG_TAG, "Config file ".concat(fulldata));

        JSONObject jsonObject = null;

            jsonObject = new JSONObject(fulldata);
            JSONObject jsonObj2 = jsonObject.optJSONArray("items").getJSONObject(0);
            String configtime = jsonObj2.getString("configfile");
            String playlisttime = jsonObj2.getString("playlistfile");
            String songlisttime = jsonObj2.getString("songlistfile");
            if(!this.configtime.equals(configtime)){
                this.configtime = configtime;
              //  dbHandler.Update_StatsRecord(new Datastats("config",configtime));
            }
            if(!this.playlisttime.equals(playlisttime)){
                this.playlisttime = playlisttime;
               // dbHandler.Update_StatsRecord(new Datastats("playlist",playlisttime));
            }
            if(!this.songlisttime.equals(songlisttime)){
                this.songlisttime = songlisttime;
              //  dbHandler.Update_StatsRecord(new Datastats("songlist",songlisttime));
                new DownloadFileFromURL().execute(songfile_url);
                Log.d(LOG_TAG, "Getting Songlist from Net");

            }


        myFile.delete();


    }


    //---------------------------------------------------class MusicIntentReceiver ------------------------------------------------

    private class MusicIntentReceiver extends BroadcastReceiver {

        private boolean headsetConnected = false;

        @Override public void onReceive(Context context, Intent intent) {

            Intent newIntent = new Intent();
            newIntent.setClassName("in.krititech.jagat.gramin", "in.krititech.jagat.gramin.MainActivity");
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(newIntent);

            //------------------------Audio Mode--------------------------
            AudioManager am;
            am= (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);
            //For Normal mode
            //am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);

            //For Silent mode
            //   am.setRingerMode(AudioManager.RINGER_MODE_SILENT);

            //For Vibrate mode
            //am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);

            //-------------------------------Detect Headset ------------------------------
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        Log.d(LOG_TAG, "Headset is unplugged");
                        Log.d(LOG_TAG, "Ringer Mode :".concat(String.valueOf(audiomode)));
                        am.setRingerMode(audiomode);
                        headsetisConnected = false;
                        stopTimer();
                        try {
                            playpauseSong();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }





                        break;
                    case 1:
                        Log.d(LOG_TAG, "Headset is plugged--");
                        Log.d(LOG_TAG, "Ringer Mode :".concat(String.valueOf(AudioManager.RINGER_MODE_SILENT)));
                        am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                        headsetisConnected = true;
                        startTimer();

                        break;
                    default:
                        Log.d(LOG_TAG, "I have no idea what the headset state is");
                }
            }else
            if (intent.hasExtra("state")){
                if (headsetConnected && intent.getIntExtra("state", 0) == 0){
                    headsetConnected = false;
                    Log.d(LOG_TAG, "New Headset is unplugged");
                    Log.d(LOG_TAG, "Ringer Mode :".concat(String.valueOf(audiomode)));
                    am.setRingerMode(audiomode);
                    headsetisConnected = false;
                    stopTimer();




                } else if (!headsetConnected && intent.getIntExtra("state", 0) == 1){
                    headsetConnected = true;
                    Log.d(LOG_TAG, "New Headset is plugged");
                    Log.d(LOG_TAG, "Ringer Mode :".concat(String.valueOf(AudioManager.RINGER_MODE_SILENT)));
                    am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                    headsetisConnected = true;
                    startTimer();
                    //intent = getIntent(getApplicationContext(), MainActivity.class);
                }
            }

            //--------------------------------------------------------
        }
    }

    private void bringApplicationToFront()
    {

        Log.d(LOG_TAG, "====Bringging Application to Front====");

       /* Intent it = new Intent("android.intent.action.MAIN");
        it.setComponent(new ComponentName(context.getPackageName(), DisplayMessageActivity.class.getName()));
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(it);
*/
      //  if(activityVisible == false  ) {

            if (intent != null) {
                //模拟点击桌面图标的启动参数
                intent.setPackage(null);
                // intent.setSourceBounds(new Rect(804,378, 1068, 657));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                context.startActivity(intent);


            }
       // }

    }





private boolean detectheaset(Context context, Intent intent){
     boolean headsetConnected = false;
    AudioManager am;
    am= (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);

   /* t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if(status != TextToSpeech.ERROR) {
                t1.setLanguage(Locale.UK);
            }
        }
    });

    String toSpeak = "Hello how are you";
    t1.speak(toSpeak,1,null,null);
    //t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);

    */


    //-------------------------------Detect Headset ------------------------------

    if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
        int state = intent.getIntExtra("state", -1);
        switch (state) {
            case 0:
                Log.d(LOG_TAG, "Headset is unplugged");
                Log.d(LOG_TAG, "Ringer Mode :".concat(String.valueOf(audiomode)));
                am.setRingerMode(audiomode);
                headsetisConnected = false;
                try {
                    playpauseSong();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                stopTimer();
                break;
            case 1:
                bringApplicationToFront();
                Log.d(LOG_TAG, "Headset is plugged--");
                Log.d(LOG_TAG, "Ringer Mode :".concat(String.valueOf(AudioManager.RINGER_MODE_SILENT)));
                am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                headsetisConnected = true;

                startTimer();


                break;
            default:
                Log.d(LOG_TAG, "I have no idea what the headset state is");
        }
    }else
    if (intent.hasExtra("state")){
        if (headsetConnected && intent.getIntExtra("state", 0) == 0){
            headsetConnected = false;
            Log.d(LOG_TAG, "New Headset is unplugged");
            Log.d(LOG_TAG, "Ringer Mode :".concat(String.valueOf(audiomode)));
            am.setRingerMode(audiomode);
            headsetisConnected = false;
            stopTimer();
        } else if (!headsetConnected && intent.getIntExtra("state", 0) == 1){
            headsetConnected = true;
            Log.d(LOG_TAG, "New Headset is plugged");
            bringApplicationToFront();
            Log.d(LOG_TAG, "Ringer Mode :".concat(String.valueOf(AudioManager.RINGER_MODE_SILENT)));
            am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
            headsetisConnected = true;
            startTimer();
            //intent = getIntent(getApplicationContext(), MainActivity.class);
        }
    }

    Log.d(LOG_TAG, "Headset Plugged Status :"+ String.valueOf(headsetConnected));
return headsetConnected;
    //--------------------------------------------------------
}

    private boolean checkfolder(){

        File myDir = new File(Environment.getExternalStorageDirectory().getPath(), folder_name);

        if (!myDir.exists()) {
            Log.e(LOG_TAG, folder_name.toString() + " Folder doesnot not exist");
            isfolder = false;
            if (!myDir.mkdirs()) {
                Log.e(LOG_TAG,  folder_name.toString()+" Folder Cannot Create");
                isfolder = false ;

            }else {
                Log.e(LOG_TAG, folder_name.toString() + " Folder created");
                isfolder = true;
            }

        }else {
            Log.e(LOG_TAG, folder_name.toString() + "Folder Exists".concat(myDir.toString()));
            isfolder = true;
        }

        return isfolder;
    }


    private boolean checkconfig(){

        String fname = "config.json";
        String configfilelink = is_register+"deviceid=" + this.android_id + "&phone=" + this.phone_no + "&imei=" + this.imei_no + "/config.json";

        configfile = sdcardpath + "/".concat(folder_name) + "/config.json";
        File myFile = new File(configfile);
        if (!myFile.exists()) {

            isconfigfile = false;

            Log.d(LOG_TAG, "Config file Download url " + configfilelink);
            new DownloadFileFromURL().execute(configfilelink);

        }else {
            isconfigfile = true;
        }

      return   isconfigfile;
    }

    private void readconfig() throws IOException, JSONException {

        configfile = sdcardpath + "/".concat(folder_name) + "/config.json";
        File myFile = new File(configfile);

        Log.d(LOG_TAG, "Config file exists ");


        String fulldata = "";

        if (myFile.exists()) {
            //file.delete ();
            BufferedReader br = null;

            try {
                br = new BufferedReader(new FileReader(myFile));
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            String line = "";

            while ((line = br.readLine()) != null) {
                //Do something here  Read Playlist.txt
                fulldata = fulldata.concat(line);

            }


            fulldata = fulldata.substring(2, (fulldata.length() - 4)).concat("}");
            Log.e(LOG_TAG, "Config file ".concat(fulldata));
String  date = String.valueOf(getDate())+" "+String .valueOf(getTime());
            JSONObject jsonObject = new JSONObject(fulldata);
            JSONObject jsonObj2 = jsonObject.optJSONArray("items").getJSONObject(0);
            this.accountid = jsonObj2.getString("account_id");
            this.busname = jsonObj2.getString("account_id");
            this.bustime = jsonObj2.getString("account_id");
            this.busroute = jsonObj2.getString("account_id");
            this.duration = jsonObj2.getString("account_id");
            this.ytd = jsonObj2.getString("account_id");
            this.mtd = jsonObj2.getString("account_id");
dbHandler.Update_StatsRecord(new Datastats("config",date));
            //this.accountid = "32";
        }
        file_url = server_url+"accid="+this.accountid+"/playlist.txt";
        songfile_url = serveraddr+"checkplaylist/?jsoncallback=?&accid="+this.accountid+"/songlist.txt";
        songackurl = serveraddr+"download/?jsoncallback=?&accid="+this.accountid+"&deviceid="+this.android_id+"&songid=";
    }



private void displaysetup(){



    TextView busname = (TextView)findViewById(R.id.busname);
    busname.setText(padRight("Bus Name",20)+this.busname);

    TextView bustime = (TextView)findViewById(R.id.bustime);
    bustime.setText(padRight("Bus Time",20)+this.bustime);

    TextView busroute = (TextView)findViewById(R.id.busroute);
    busroute.setText(padRight("Bus Route",20)+this.busroute);

    TextView busnoofseats = (TextView)findViewById(R.id.busnoofseats);
    busnoofseats.setText(padRight("No. of Seats", 40) + this.busroute);

    TextView duration = (TextView)findViewById(R.id.duration);
    duration.setText(padRight("Duration",20)+this.busroute);

    TextView durationmtd = (TextView)findViewById(R.id.durationmtd);
    durationmtd.setText(padRight("Duration MTD", 20) + this.busroute);

    TextView durationytd = (TextView)findViewById(R.id.durationytd);
    durationytd.setText(padRight("Duration YTD",20)+this.busroute);

}

    public static String padRight(String s, int n) {
        Integer i = n-s.length();
        if(i<0)
            i=0;
        return String.format("%1$-" + i + "s", s);
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }


    public void downloadsongs() throws IOException {
        // Get the directory for the user's public pictures directory.
        File myDir = new File(Environment.getExternalStorageDirectory().getPath(), folder_name);



        if (myDir.exists()) {

            String fname = "songlist.txt";
            String fulldata = "";
            File file = new File(myDir, fname);
            if (file.exists()) {
                //file.delete ();
                BufferedReader br = null;

                try {
                    br = new BufferedReader(new FileReader(file));
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                String line = "";

                while ((line = br.readLine()) != null) {
                    //Do something here  Read Playlist.txt
                    fulldata = fulldata.concat(line);

                }


                fulldata = fulldata.substring(2,(fulldata.length()-4)).concat("}");
                try {






                    JSONObject jsonObj  = new JSONObject(fulldata);
                    JSONObject jsonObj2 = jsonObj.optJSONArray("items").getJSONObject(0).getJSONObject("playlist");
                    JSONArray jsonarray = jsonObj2.names();
                    downloadsongs = new String [jsonObj2.length()];
                   // dbHandler.Delete_SongRecord(1);
                    for (int i=1;   i< jsonObj2.length()+1; i++) {

                        JSONObject jsonObj3 = jsonObj2.getJSONObject(String.valueOf(i));
                        String name = jsonObj3.getString("song_name");
                        String songId = jsonObj3.getString("song_id");
                        String path = sdcardpath+"/"+folder_name+"/"+name;
                        String downloadpath ;//= download_url.concat(jsonObj3.getString("song"))+"/"+name.substring(name.lastIndexOf("/") + 1);
                        downloadpath = download_url+"UPLOAD/".concat(jsonObj3.getString("song_name"));
                      //  dbHandler.Add_SongRecord(new Datasong(Integer.valueOf(songId),name));
                        File checkfile = new File(path);
                        if(!checkfile.exists()){

                            Log.e(LOG_TAG," file not exists "+path);
                            //new DownloadFileFromURL().execute(downloadpath);
                            new DownloadManager(this).execute(downloadpath, name.substring(name.lastIndexOf("/") + 1),songId);
                        }else {

                            Log.e(LOG_TAG," file exists "+name.substring(name.lastIndexOf("/") + 1));
                        }
                    }



                    file.delete();



                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(LOG_TAG, "Song read error " + e.toString());
                    file.delete();
                }

                //Log.e(LOG_TAG, "Jason  Line ".concat(fulldata.substring(600)));

            } else {

                // -------------------Reading from internet-------------------------



                new DownloadFileFromURL().execute(songfile_url);
                Log.d(LOG_TAG, "Getting Playlist from Net");



                //------------------------------------------------------------------

            }
        }

    }


    private class DownloadManager extends AsyncTask<String, Integer, Drawable>
    {

        private Drawable d;
        private HttpURLConnection conn;
        private InputStream stream; //to read
        private ByteArrayOutputStream out; //to write
        private Context mCtx;

        private double fileSize;
        private double downloaded; // number of bytes downloaded
        private int status = DOWNLOADING; //status of current process
        private String file ;
        private String songid;

       // private ProgressDialog progressDialog;

        private static final int MAX_BUFFER_SIZE = 1024; //1kb
        private static final int DOWNLOADING = 0;
        private static final int COMPLETE = 1;

        public DownloadManager(Context ctx)
        {
            d          = null;
            conn       = null;
            fileSize   = 0;
            downloaded = 0;
            status     = DOWNLOADING;
            mCtx       = ctx;
        }

        public boolean isOnline()
        {
            try
            {
                ConnectivityManager cm = (ConnectivityManager)mCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
                return cm.getActiveNetworkInfo().isConnectedOrConnecting();
            }
            catch (Exception e)
            {
                return false;
            }
        }

        @Override
        protected Drawable doInBackground(String... url)
        {
            try
            {

                String filename = url[1];
                file = filename ;
                File myfilecheck =new  File(sdcardpath + "/GraminPrachar/" + filename);

                if(!myfilecheck.exists()) {
                    songid = url[2];
                    if (isOnline()) {
                        Log.d(LOG_TAG, "download  song No Exception Playlist " + url[0].toString());
                        conn = (HttpURLConnection) new URL(url[0].replace(" ", "%20")).openConnection();
                        fileSize = conn.getContentLength();
                        out = new ByteArrayOutputStream((int) fileSize);
                        conn.connect();
                        Log.d(LOG_TAG, "download  song No Exception " + url[0].toString());
                        stream = conn.getInputStream();
                        // loop with step
                        while (status == DOWNLOADING) {
                            byte buffer[];

                            if (fileSize - downloaded > MAX_BUFFER_SIZE) {
                                buffer = new byte[MAX_BUFFER_SIZE];
                            } else {
                                buffer = new byte[(int) (fileSize - downloaded)];
                            }
                            int read = stream.read(buffer);

                            if (read == -1) {
                                publishProgress(100);
                                break;
                            }
                            // writing to buffer
                            out.write(buffer, 0, read);
                            downloaded += read;
                            // update progress bar
                            publishProgress((int) ((downloaded / fileSize) * 100));
                        } // end of while

                        if (status == DOWNLOADING) {
                            status = COMPLETE;
                        }
                        try {
                            if (filename != "nofile") {

                                FileOutputStream fos = new FileOutputStream(sdcardpath + "/GraminPrachar/" + filename);
                                fos.write(out.toByteArray());
                                fos.close();
                                Log.e(LOG_TAG,"DOWNLOADED FILE PATH : "+sdcardpath + "/GraminPrachar/" + filename);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            return null;
                        }

                        d = Drawable.createFromStream((InputStream) new ByteArrayInputStream(out.toByteArray()), "filename");
                        return d;
                    } // end of if isOnline
                    else {
                        return null;
                    }

                } else
                    return null;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Log.d(LOG_TAG, "download  song Exception " + e.toString());
                Log.d(LOG_TAG, "download  song Exception " + url[0].toString());
                Log.d(LOG_TAG, "download  song Exception " + url[1].toString());
                Log.d(LOG_TAG, "download  song Exception " + url[2].toString());
                return null;
            }// end of catch
        } // end of class DownloadManager()

        @Override
        protected void onProgressUpdate(Integer... changed)
        {
           // progressDialog.setProgress(changed[0]);
        }

        @Override
        protected void onPreExecute()
        {
            /*progressDialog = new ProgressDialog(/*ShowContent.this); // your activity
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMessage("Downloading ...");
            progressDialog.setCancelable(false);
            progressDialog.show();*/
        }

        @Override
        protected void onPostExecute(Drawable result)
        {
           // progressDialog.dismiss();
            // do something

            try {

                //Log.d(LOG_TAG, "Hit URL for song " + songackurl.concat(songid).replace(" ", "%20"));

                new UpdateStatus().execute(songackurl.concat(songid)+"/download.php".replace(" ", "%20"));

            }catch (Exception e){
                Log.d(LOG_TAG, "Hit URL for song " + file +" "+ e.toString());
            }

            Log.d(LOG_TAG, "download completed for song " + file);
        }
    }

    private boolean Checkplaylistfile(){

        String fname = "playlist.txt";
        String playlistfilelink = serveraddr+"alllist/?jsoncallback=?&accid="+this.accountid+"/playlist.txt";
        String playlistfile = sdcardpath + "/".concat(folder_name) + "/playlist.txt";
        File myFile = new File(playlistfile);
        if (!myFile.exists()) {

            isplaylistfile = false;

            Log.d(LOG_TAG, "Playlist file Download url " + playlistfilelink);
            new DownloadFileFromURL().execute(playlistfilelink, "playlist.txt","11111");
            //new DownloadManager(this).execute(playlistfilelink, "playlist.txt","11111");


        }else {
            Log.d(LOG_TAG, "Playlist file Exists ");
            isplaylistfile = true;
        }


        return isplaylistfile;
    }

    public void readplaylist() throws IOException {

            String fulldata = "";
            File file = new File(sdcardpath+"/"+folder_name+"/playlist.txt");

                BufferedReader br = null;

                try {
                    br = new BufferedReader(new FileReader(file));

                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                String line = "";

                while ((line = br.readLine()) != null) {
                    fulldata = fulldata.concat(line);

                }


                fulldata = fulldata.substring(2,(fulldata.length()-4)).concat("}");
                try {

                    JSONObject jsonObj  = new JSONObject(fulldata);
                    JSONObject jsonObj2 = jsonObj.optJSONArray("items").getJSONObject(0).getJSONObject("playlist");
                    Integer count = jsonObj2.length()+1 ;
                    this.playlistsongs = new String[count-1];
                    this.playlistmAudioPath = new String[count-1];
                    String  date = getDate().toString();
                   // dbHandler.Delete_PlaylistRecord(date);
                    this.songsarray = new String[count-1][6];
                    for (int i=1;   i <  count; i++) {

                        JSONObject jsonObj3 = jsonObj2.getJSONObject(String.valueOf(i));
                        String name = jsonObj3.getString("song_name");
                        String type = jsonObj3.getString("type");
                        String start_time = jsonObj3.getString("start_time");
                        String end_time = jsonObj3.getString("end_time");
                        String duration = jsonObj3.getString("duration");
                        String songid = jsonObj3.getString("song_id");
                        String path = sdcardpath.concat(name).replace("GRAMINPRACHAR", "GraminPrachar");
                        String playlistid = jsonObj3.getString("playlist_id");
                        //Log.d(LOG_TAG, "Playlist path "+ path);
                        String song = name.substring(name.lastIndexOf("/")+1);
                        File checkfile = new File(path);
                        //if(checkfile.exists()){
try {
    this.playlistmAudioPath[i-1] = path;
    this.playlistsongs[i-1] = song;
    this.songsarray[i-1][NAME]=song;
    this.songsarray[i-1][PATH]=path;
    this.songsarray[i-1][TYPE]=type;
    this.songsarray[i-1][DURATION]=duration;
    this.songsarray[i-1][START_TIME]=start_time;
    this.songsarray[i-1][SONGID]=songid;

    dbHandler.Add_PlaylistRecord(new Dataplaylist(playlistid,date,start_time,end_time,duration,type,name,path));
}catch (Exception e){
    Log.d(LOG_TAG, "Playlist exception "+ e.toString());

}

                 //       }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(LOG_TAG, "Song read error 1: " + e.toString());
                    file.delete();
                    this.songsarray = new String[1][6];
                }


                //Log.e(LOG_TAG, "Jason  Line ".concat(fulldata.substring(600)));





    }

    public void stopTimer()  {
        mMediaplayerMute = true;
        try {
            alertspeaker();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void startTimer()  {


            mMediaplayerMute = false;
          mMediaplayeralert.setVolume(0,0);
          mMediaPlayer.setVolume(1,1);

    }

    public void updatePlayRecord()  {

        String url2 = "http://45.58.34.139/graminpracharapp/index.php/graminprachar/is_register/?jsoncallback=?&deviceid="
                + this.android_id + "&position=1&songid=1time=15 : 27&eventtype=song&status=complete";


        new DownloadFileFromURL().execute(url2);
        try {
            URL url1 = new URL(url2);

            Log.d(LOG_TAG, "URL Register :".concat(url2));

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void Playlistdisplay(){

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_1,playlistsongs );
        lv = (ListView) findViewById(R.id.listView1);
        lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lv.setSelection(2);
        lv.setSelected(true);
        lv.setItemChecked(2, true);
        lv.setAdapter(arrayAdapter);
    }


    private void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
        connectivityManagerField.setAccessible(true);
        final Object connectivityManager = connectivityManagerField.get(conman);
        final Class connectivityManagerClass =  Class.forName(connectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
    }



    private void canToggleGPS(boolean status) {
        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        //Enable / Disable GPS
        intent.putExtra("enabled", status);
        context.sendBroadcast(intent);

    }
    public void turnGPSOn()
    {
        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", true);
        this.sendBroadcast(intent);

        String provider = Settings.Secure.getString(this.getContentResolver(), String.valueOf(Settings.Secure.LOCATION_MODE_HIGH_ACCURACY));
        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            this.sendBroadcast(poke);


        }
    }
    // automatic turn off the gps
    public void turnGPSOff()
    {
        String provider = Settings.Secure.getString(this.getContentResolver(), String.valueOf(Settings.Secure.LOCATION_MODE_HIGH_ACCURACY));
        if(provider.contains("gps")){ //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            this.sendBroadcast(poke);
        }
    }



}





