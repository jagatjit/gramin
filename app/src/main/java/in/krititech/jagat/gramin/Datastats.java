package in.krititech.jagat.gramin;

/**
 * Created by jagat on 07/04/16.
 */
public class Datastats {
    public int _id;
    public String  _statname;
    public String _statvalue;

    public Datastats(int id,String statname,String statvalue){
        this._id = id;
        this._statname = statname;
        this._statvalue = statvalue;
    }

    public Datastats(String statname,String statvalue){

        this._statname = statname;
        this._statvalue = statvalue;
    }

    public Datastats() {

    }

    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    // getting ID
    public String getStatname() {
        return this._statname;
    }

    // setting id
    public void setStatname(String  statname) {
        this._statname= statname;
    }

    // getting ID
    public String getStatvalue() {
        return this._statvalue;
    }

    // setting id
    public void setStatvalue(String  statvalue) {
        this._statvalue= statvalue;
    }
}
