package in.krititech.jagat.gramin;

import java.sql.Date;
import java.sql.Time;

/**
 * Created by jagat on 01/04/16.
 */
public class Datarecord {

    // private variables
    public int _id;
    public Date _date;
    public Time _time;
    public String _type;
    public String _status;
    public String _eventid;
    public Time _duration,_mtd,_ytd;


    // constructor
    public Datarecord(int id, Date date, Time time, String type,String status,String eventid,Time duration,Time mtd,Time ytd ) {

        this._id = id;
        this._date = date;
        this._time = time;
        this._type = type;
        this._status = status;
        this._eventid = eventid;
        this._duration = duration;
        this._mtd = mtd;
        this._ytd = ytd;

    }



    // constructor
    public Datarecord(Date date, Time time, String type,String status,String eventid,Time duration,Time mtd,Time ytd ) {

        this._date = date;
        this._time = time;
        this._type = type;
        this._status = status;
        this._eventid = eventid;
        this._duration = duration;
        this._mtd = mtd;
        this._ytd = ytd;

    }

    public Datarecord() {
        
    }

    public Datarecord(int id, String string, String string1, String string2, String string3, String string4, String string5, String string6, String string7) {
    }

    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    // getting date
    public Date getDate() {
        return this._date;
    }

    // setting date
    public void setDate(Date date) {
        this._date = date;
    }

    // getting time
    public Time getTime() {
        return this._time;
    }

    // setting time
    public void setTime(Time time) {
        this._time = time;
    }

    // getting type
    public String getType() {
        return this._type;
    }

    // setting type
    public void setType(String type) {
        this._type = type;
    }

    // getting status
    public String getStatus() {
        return this._status;
    }

    // setting status
    public void setStatus(String status) {this._status =status;}

    // getting event
    public String getEventid() {
        return this._eventid;
    }

    // setting event
    public void setEventid(String eventid) {this._eventid = eventid;}

    // getting duration
    public Time getDuration() {
        return this._duration;
    }

    // setting duration
    public void setDuration(Time duration) {this._duration = duration;}

    // getting mtd
    public Time getMtd() {
        return this._mtd;
    }

    // setting mtd
    public void setMtd(Time mtd) {this._duration = mtd;}

    // getting ytd
    public Time getYtd() {
        return this._ytd;
    }

    // setting ytd
    public void setYtd(Time ytd) {
        this._ytd = ytd;
    }

}
