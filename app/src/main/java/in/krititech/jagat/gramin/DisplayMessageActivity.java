package in.krititech.jagat.gramin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import android.content.*;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Integer version = Build.VERSION.SDK_INT;
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE).concat(String.valueOf(version));

        TextView textView= new TextView(this);
        textView.setTextSize(40);
        textView.setText(message);
        RelativeLayout layout= (RelativeLayout) findViewById(R.id.content);
        layout.addView(textView);


        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.pref_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedprefeditor  = sharedPref.edit();

        sharedprefeditor.putString(getString(R.string.pref_key), version.toString());
        sharedprefeditor.commit();

       /* SharedPreferences sharedPref1 = getActivity().getPreferences(Context.MODE_PRIVATE);
        Integer defaultvalue = sharedPref.getString(R.string.pref_key);
        Button button = (Button) findViewById(R.id.pref);
        button.setText(defaultvalue);
*/



    }



}
