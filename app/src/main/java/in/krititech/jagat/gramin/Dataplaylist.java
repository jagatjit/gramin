package in.krititech.jagat.gramin;

/**
 * Created by jagat on 07/04/16.
 */
public class Dataplaylist {

    public int _id;
    public String  _playlistid;
    public String _date;
    public String _starttime;
    public String _endtime;
    public String _duration;
    public String _type;
    public String _songname;
    public String _path;
    public String _svrid;


    public Dataplaylist(int id,String playlistid,String date,String starttime,String endtime, String duration,String type,String songname,String path){

        this._id = id;
        this._playlistid = playlistid;
        this._date= date;
        this._starttime = starttime;
        this._endtime = endtime;
        this._duration = duration;
        this._type= type;
        this._songname = songname;
        this._path = path;
    }

    public Dataplaylist(String playlistid,String date,String starttime,String endtime, String duration,String type,String songname,String path){


        this._playlistid = playlistid;
        this._date= date;
        this._starttime = starttime;
        this._endtime = endtime;
        this._duration = duration;
        this._type= type;
        this._songname = songname;
        this._path = path;
    }

    public Dataplaylist() {

    }


    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    // getting ID
    public String getplaylistID() {
        return this._playlistid;
    }

    // setting id
    public void setplaylistID(String  playlistid) {
        this._playlistid = playlistid;
    }

    // getting ID
    public String getDate() {
        return this._date;
    }

    // setting id
    public void setDate(String  date) {
        this._date = date;
    }

    // getting ID
    public String getStarttime() {
        return this._starttime;
    }

    // setting id
    public void setStarttime(String  starttime) {
        this._starttime = starttime;
    }

    // getting ID
    public String getEndtime() {
        return this._endtime;
    }

    // setting id
    public void setEndtime(String  endtime) {
        this._endtime = endtime;
    }

    // getting ID
    public String getDuration() {
        return this._duration;
    }

    // setting id
    public void setDuration(String  duration) {
        this._duration = duration;
    }

    // getting ID
    public String getType() {
        return this._type;
    }

    // setting id
    public void setType(String  type) {
        this._type = type;
    }

    // getting ID
    public String getSongname() {
        return this._songname;
    }

    // setting id
    public void setSongname(String  songname) {
        this._songname = songname;
    }

    // getting ID
    public String getPath() {
        return this._path;
    }

    // setting id
    public void setPath(String  path) {
        this._path = path;
    }

    // getting ID
    public String getSvrid() {
        return this._svrid;
    }

    // setting id
    public void set_svrid(String  path) {
        this._svrid = path;
    }

}
