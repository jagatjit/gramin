package in.krititech.jagat.gramin;

/**
 * Created by jagat on 07/04/16.
 */
public class Datasong {

    public int _id;
    public int _songid;
    public String _songname;

    public Datasong(int id, int songid, String songname) {

        this._id = id;
        this._songid = songid;
        this._songname = songname;


    }

    public Datasong(int songid, String songname) {

        this._songid = songid;
        this._songname = songname;

    }

    public Datasong() {

    }

    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    // getting SongID
    public int getSongID() {
        return this._songid;
    }

    // setting id
    public void setSongID(int songid) {
        this._songid = songid;
    }


    // getting Name
    public String getSongName() {
        return this._songname;
    }

    // setting id
    public void setSongName(String songname) {
        this._songname = songname;
    }

}
