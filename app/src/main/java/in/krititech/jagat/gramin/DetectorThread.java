package in.krititech.jagat.gramin;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.util.Log;


import com.musicg.api.WhistleApi;
import com.musicg.wave.WaveHeader;

import java.util.LinkedList;

//import weetech.wallpaper.utils.Debug;

public class DetectorThread extends Thread {

	private RecorderThread recorder;
	private WaveHeader waveHeader;
	private WhistleApi whistleApi;
	private Thread _thread;

	private LinkedList<Boolean> whistleResultList = new LinkedList<Boolean>();
	private int numWhistles;
	public int totalWhistlesDetected = 0;
	private int whistleCheckLength = 3;
	private int whistlePassScore = 3;
	private String LOG_TAG ="CHECKMSG";


	public DetectorThread(RecorderThread recorder) {
		this.recorder = recorder;
		AudioRecord audioRecord = recorder.getAudioRecord();

		int bitsPerSample = 0;
		if (audioRecord.getAudioFormat() == AudioFormat.ENCODING_PCM_16BIT) {
			bitsPerSample = 16;
		} else if (audioRecord.getAudioFormat() == AudioFormat.ENCODING_PCM_8BIT) {
			bitsPerSample = 8;
		}

		int channel = 0;
		// whistle detection only supports mono channel
		if (audioRecord.getChannelConfiguration() == AudioFormat.CHANNEL_IN_MONO) {
			channel = 1;
		}

		waveHeader = new WaveHeader();
		waveHeader.setChannels(channel);
		waveHeader.setBitsPerSample(bitsPerSample);
		waveHeader.setSampleRate(audioRecord.getSampleRate());
		whistleApi = new WhistleApi(waveHeader);
	}

	private void initBuffer() {
		numWhistles = 0;
		whistleResultList.clear();

		// init the first frames
		for (int i = 0; i < whistleCheckLength; i++) {
			whistleResultList.add(false);
		}
		// end init the first frames
	}

	public void start() {
		_thread = new Thread(this);
		_thread.start();
	}

	public void stopDetection() {
		_thread = null;
	}

	@Override
	public void run() {
		Log.e(LOG_TAG, "DetectorThread started...");

		try {
			byte[] buffer;
			initBuffer();

			Thread thisThread = Thread.currentThread();
			while (_thread == thisThread) {
				// detect sound
				buffer = recorder.getFrameBytes();

				// audio analyst
				if (buffer != null) {
					// sound detected
					// MainActivity.whistleValue = numWhistles;

					// whistle detection
					// System.out.println("*Whistle:");

					try {
						boolean isWhistle = whistleApi.isWhistle(buffer);
						//Log.e(LOG_TAG, "isWhistle : " + isWhistle + " " + buffer.length);

						if (whistleResultList.getFirst()) {
							numWhistles--;
						}

						whistleResultList.removeFirst();
						whistleResultList.add(isWhistle);

						if (isWhistle) {
							numWhistles++;
						}

						// Log.e(LOG_TAG, "numWhistles : " + numWhistles);

						if (numWhistles >= whistlePassScore) {
							// clear buffer
							initBuffer();
							totalWhistlesDetected++;

							Log.e(LOG_TAG, "totalWhistlesDetected : "
									+ totalWhistlesDetected);

							if (onWhistleListener != null) {
								onWhistleListener.onWhistle();
							}
						}
					} catch (Exception e) {
						Log.e(LOG_TAG, "" + e.getCause());
					}
					// end whistle detection
				} else {
					// Log.e(LOG_TAG, "no sound detected");
					// no sound detected
					if (whistleResultList.getFirst()) {
						numWhistles--;
					}
					whistleResultList.removeFirst();
					whistleResultList.add(false);

					// MainActivity.whistleValue = numWhistles;
				}
				// end audio analyst
			}

			Log.e(LOG_TAG, "Terminating detector thread...");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private OnWhistleListener onWhistleListener;

	public void setOnWhistleListener(OnWhistleListener onWhistleListener) {
		this.onWhistleListener = onWhistleListener;
		Log.e(LOG_TAG, "onWhistleListener");

	}

	public interface OnWhistleListener {
		void onWhistle();
	}

	public int getTotalWhistlesDetected() {
		return totalWhistlesDetected;
	}

}